package com.myzee.beans;

public class SpellChecker {
	
	public SpellChecker() {
		System.out.println("spellchecker constructor");
	}
	
	public void checkSpelling(String word) {
		System.out.println("spelling - " + word);
	}
}

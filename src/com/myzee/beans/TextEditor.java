package com.myzee.beans;

import org.springframework.beans.factory.annotation.Autowired;

public class TextEditor {
	private SpellChecker spellChecker;
	private String word;
	
	@Autowired
	public void setSpellChecker(SpellChecker spellChecker) {
		this.spellChecker = spellChecker;
	}
	
	public void setWord(String word) {
		this.word = word;
	}
	
	public void spellCheck() {
		spellChecker.checkSpelling(word);
	}
}

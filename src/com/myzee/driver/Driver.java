package com.myzee.driver;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.myzee.beans.TextEditor;

public class Driver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ApplicationContext ap = new ClassPathXmlApplicationContext("com/myzee/resources/spring.xml");
		TextEditor tt = (TextEditor)ap.getBean("t");
		tt.spellCheck();
	}
}
